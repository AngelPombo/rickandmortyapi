"use strict"
const divContainer = document.querySelector(".container");


createCards();



//---Funciones:

async function getCharacters () {
    try{
        const apiUrl = "https://rickandmortyapi.com/api/character";
        const response = await fetch(apiUrl);

        if(!response.ok){
            throw new Error("No se ha podido acceder a la data");
        }

        const data = await response.json();
        return data.results;

    } catch(e){
        console.log(e);
    }
}

function createCard (character) {

    const characterNameInput = document.querySelector("input");
    const card = document.createElement("article")
    const characterStatus = character.status.toLowerCase();

    card.innerHTML=
            `<img src="${character.image}" alt="img${character.name}">
            <h2>${character.name}</h2>
            <h3>${character.id}</h3>
            <ul>
                <li class="${characterStatus}">${character.status}</li>
                <li>${character.species}</li>
                <li>${character.type}</li>
                <li>${character.gender}</li>
                <li>${character.origin.name}</li>
                <li>${character.location.url? `<a href="${character.location.url}">${character.location.name}</a>` : character.location.name}</li>
            </ul>    
        `
        return card
};

async function createCards () {
    try{
        
        const arrayCharacters = await getCharacters();

        for(let i=0; i < arrayCharacters.length; i++){
            const card = createCard(arrayCharacters[i]);
            divContainer.appendChild(card);
        }

    } catch(e){
        console.log(e);
    }
}

